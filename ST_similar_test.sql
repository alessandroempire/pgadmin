﻿
--
-- Metodo que recibe dos geometrias 
--
create or replace function ST_mycompare (geom1 geometry, geom2 geometry) 
returns boolean as
$$
declare angle1 float;
	angle2 float;
begin
	-- raise notice 'distance %', ST_DWithin(geom1, geom2, 0.00007);
-- 	raise notice 'angle %', ST_myangle(geom1, geom2);
	return (ST_DWithin(geom1, geom2, 0.00007) and ST_myangle(geom1, geom2));
	
end;
$$ language plpgsql; 

--
-- Metodo que recibe 4 puntos 
-- Los dos primero son de la primera geometria
-- Los dos ultimos son de la segunda geometria
-- (x1, y1) - (x2, y2)
--
create or replace function ST_myanglediff(x1 float, y1 float, x2 float, y2 float,
					x3 float, y3 float, x4 float, y4 float)
	returns float as
	$$
	begin

		return degrees (
			ST_Azimuth(
				ST_Point(x1, y1),
				ST_point(x2, y2)
				)
			-
			ST_Azimuth(
				ST_Point(x3, y3),
				ST_Point(x4, y4)
				)
			);
	end;
	$$ language plpgsql; 


create or replace function ST_angleDiff(x1 float, y1 float, x2 float, y2 float,
					x3 float, y3 float, x4 float, y4 float)
	returns float as 
	$$
	declare
		 angle1 float;
		 angle2 float; 
		 diff   float;
	begin
		
		select (ST_Azimuth( ST_Point(x1, y1), ST_point(x2, y2) )) into angle1;
		select (ST_Azimuth( ST_Point(x3, y3), ST_point(x4, y4) )) into angle2;
		SELECT degrees (angle2 - angle1) INTO diff;
		CASE
		    WHEN diff >   180 THEN RETURN diff - 360;
		    WHEN diff <= -180 THEN RETURN diff + 360;
		    ELSE                   RETURN diff;
		    END CASE;
	end;
	$$ language plpgsql;


--- La primera geometria corresponde al mapa graph_streets_mcbo
--- La segunda geometria corresponde al mapa graph_streets_osm_new_p_mcbo 
-- 	Estrategia:
-- 	Tengo el geometry
-- 	OBtener both_dir de cada geometry de la respectiva tabla
-- 
-- 	Luego obtener los puntos x1, y1 source
-- 	      obsysssytener los puntos x2, y2 target
-- 
-- 	Con estos puntos y segun el both_dir
-- 	    Creo los angulos
-- 
-- 	Nota: Ambas son both dir 
-- 		revisar ambos angulos
-- 	      Si una es both dir y la otra no
-- 		Si alguno de los angulos cumple
-- 	      Si ambos No son both dir
-- 		Si cumple el angulo

create or replace function ST_miangle(geom1 geometry, geom2 geometry)
    returns boolean as
    $$ 
    declare
        ambas_dir_mcbo        boolean;
        ambas_dir_osm         boolean;
    begin

    --encontrar el heading de cada geometry
    select both_dir into ambas_dir_mcbo
    from traffic.graph_streets_p_mcbo
    where the_geom = geom1;

    select both_dir into ambas_dir_osm
    from traffic.graph_streets_osm_new_p_mcbo
    where the_geom = geom2;

    -- ST_angleDiff(st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
    --          st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2)
    --          );
    if (not ambas_dir_mcbo and not ambas_dir_osm)
    then 
        raise notice 'agle dif %',   (ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) )   );
    elsif (ambas_dir_mcbo and not ambas_dir_osm)
    then raise notice 'agle dif %',   ( least (
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) ),
                ST_angleDiff( st_xmin(geom1), st_ymin(geom1), st_xmax(geom1), st_ymax(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) )
                ));

    elseif (not ambas_dir_mcbo and ambas_dir_osm)
    then raise notice 'agle dif %',   ( least (
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) ),
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmin(geom2), st_ymin(geom2),st_xmax(geom2), st_ymax(geom2) )
                )  );

    elseif (ambas_dir_osm and ambas_dir_mcbo)
    then raise notice 'agle dif %',   ( least (
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) ),
                ST_angleDiff( st_xmin(geom1), st_ymin(geom1), st_xmax(geom1), st_ymax(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) ),
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmin(geom2), st_ymin(geom2),st_xmax(geom2), st_ymax(geom2) ),
                ST_angleDiff( st_xmin(geom1), st_ymin(geom1), st_xmax(geom1), st_ymax(geom1),
                                    st_xmin(geom2), st_ymin(geom2),st_xmax(geom2), st_ymax(geom2) )
                )  );
    end if;




    if (not ambas_dir_mcbo and not ambas_dir_osm)
    then 
        return (ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) ) < 0.1);
    elsif (ambas_dir_mcbo and not ambas_dir_osm)
    then return ( least (
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) ),
                ST_angleDiff( st_xmin(geom1), st_ymin(geom1), st_xmax(geom1), st_ymax(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) )
                )< 0.1);

    elseif (not ambas_dir_mcbo and ambas_dir_osm)
    then return ( least (
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) ),
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmin(geom2), st_ymin(geom2),st_xmax(geom2), st_ymax(geom2) )
                )< 0.1);

    elseif (ambas_dir_osm and ambas_dir_mcbo)
    then return ( least (
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) ),
                ST_angleDiff( st_xmin(geom1), st_ymin(geom1), st_xmax(geom1), st_ymax(geom1),
                                    st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2) ),
                ST_angleDiff( st_xmax(geom1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
                                    st_xmin(geom2), st_ymin(geom2),st_xmax(geom2), st_ymax(geom2) ),
                ST_angleDiff( st_xmin(geom1), st_ymin(geom1), st_xmax(geom1), st_ymax(geom1),
                                    st_xmin(geom2), st_ymin(geom2),st_xmax(geom2), st_ymax(geom2) )
                )< 0.1);
    end if;

    end;
    $$ language plpgsql;


create or replace function ST_myangle(geom1 geometry, geom2 geometry)
returns boolean as
$$
declare 
    ambas_dir_mcbo        boolean;
    ambas_dir_osm         boolean;
    s_x1_mcbo             double precision;
    s_y1_mcbo             double precision;
    t_x1_mcbo             double precision;
    t_y1_mcbo             double precision;
    s_x1_osm              double precision;
    s_y1_osm              double precision;
    t_x1_osm              double precision;
    t_y1_osm              double precision;
begin

    --encontrar el heading de cada geometry
    select both_dir into ambas_dir_mcbo
    from traffic.graph_streets_p_mcbo
    where the_geom = geom1;

    select both_dir into ambas_dir_osm
    from traffic.graph_streets_osm_new_p_mcbo
    where the_geom = geom2;

    --Obtener los puntos x1, y1, x2, y2 del mapa de graph_streets_mcbo
    -- x1 y1 es source
    with target1 as (
                select target 
                from traffic.graph_streets_p_mcbo
                where the_geom = geom1
            ),
         source1 as (
                select source 
                from traffic.graph_streets_p_mcbo
                where the_geom = geom1
            ),
         coort as (
            select x1, y1
            from traffic.graph_streets_p_mcbo
            where target = (select target from target1)
              ),
         coors as ( select x2, y2
            from traffic.graph_streets_p_mcbo
            where source = (select source from source1)
                  )      
    select coors.x2, coors.y2, coort.x1, coort.y1
    into s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo
    from coort, coors
    ;

--  raise notice 'angulos % -- % -- % -- %', s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo;

    -- Obtener los puntos x1,y1, x2, y2 de graph_streets_osm_new_p_mcbo
    --
    with target1 as (
                select target 
                from traffic.graph_streets_osm_new_p_mcbo
                where the_geom = geom2
            ),
         source1 as (
                select source 
                from traffic.graph_streets_osm_new_p_mcbo
                where the_geom = geom2
            ),
         coort as (
            select x1, y1
            from traffic.graph_streets_osm_new_p_mcbo
            where target = (select target from target1)
              ),
         coors as ( select x2, y2
            from traffic.graph_streets_osm_new_p_mcbo
            where source = (select source from source1)
                  )      
    select coors.x2, coors.y2, coort.x1, coort.y1
    into s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm
    from coort, coors
    ;

--  raise notice 'angulos % -- % -- % -- %', s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm;


    if ((not ambas_dir_mcbo) and (not ambas_dir_osm))
    then raise notice 'diff %',    ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                 s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm)) < 0.1 );

    elsif ((not ambas_dir_mcbo) and (ambas_dir_osm) )
    then raise notice 'diff %',    (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                          s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                          (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                          t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm )) 
                         ) 
                  ) < 0.1 ); 
    elsif ((ambas_dir_mcbo) and (not ambas_dir_osm))
    then raise notice 'diff %',    ((LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                         (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ) 
                        )
                 ) < 0.1 ); 
    elsif  ((ambas_dir_mcbo) and (ambas_dir_osm))
    then raise notice 'diff %',    (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                     s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                    (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                    s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                    (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                    t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm) ),
                    (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo,
                                    t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm ))
                )
          )  );
    else 
        raise notice 'diff %',    false;
    end if;

    

    
    if ((not ambas_dir_mcbo) and (not ambas_dir_osm))
    then return ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                 s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm)) < 0.1 );

    elsif ((not ambas_dir_mcbo) and (ambas_dir_osm) )
    then return (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                          s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                          (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                          t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm )) 
                         ) 
                  ) < 0.1 ); 
    elsif ((ambas_dir_mcbo) and (not ambas_dir_osm))
    then return ((LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                         (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ) 
                        )
                 ) < 0.1 ); 
    elsif  ((ambas_dir_mcbo) and (ambas_dir_osm))
    then return (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                     s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                    (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                    s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                    (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                    t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm) ),
                    (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo,
                                    t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm ))
                )
          ) < 1 );
    else 
        return false;
    end if;

--  angle1 := ST_angleDiff(st_xmax(geohere source = t or target = t;m1), st_ymax(geom1), st_xmin(geom1), st_ymin(geom1),
--              st_xmax(geom2), st_ymax(geom2),st_xmin(geom2), st_ymin(geom2)
--              );

--  raise notice 'angle diff %', angle1;

--  raise notice 't or f: %', angle1 < 0.1;

--Nota menor a 0.1 es buena medida

end;
$$ language plpgsql;


-- select (ST_myangle ( (select the_geom
-- 		  from traffic.graph_streets_p_mcbo
-- 		  where id =1000137561)
-- 		  ,
-- 		  (select the_geom
-- 		  from traffic.graph_streets_osm_new_p_mcbo
-- 		  where id =508446 )
-- 		));

---------------------------------------------------------------------------------------------------------------------


create or replace function ST_myangle222(geom1 geometry, geom2 geometry)
returns boolean as
$$
declare 
    ambas_dir_mcbo        boolean;
    ambas_dir_osm         boolean;
    s_x1_mcbo             double precision;
    s_y1_mcbo             double precision;
    t_x1_mcbo             double precision;
    t_y1_mcbo             double precision;
    s_x1_osm              double precision;
    s_y1_osm              double precision;
    t_x1_osm              double precision;
    t_y1_osm              double precision;
begin

    --encontrar el heading de cada geometry
    select both_dir into ambas_dir_mcbo
    from traffic.graph_streets_p_mcbo
    where the_geom = geom1;

    select both_dir into ambas_dir_osm
    from traffic.graph_streets_osm_new_p_mcbo
    where the_geom = geom2;

    --Obtener los puntos x1, y1, x2, y2 del mapa de graph_streets_mcbo
    -- x1 y1 es source
   
    select x1, y1, x2, y2
    into s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo
    from traffic.graph_streets_p_mcbo
    where the_geom = geom1;

--  raise notice 'angulos % -- % -- % -- %', s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo;

    -- Obtener los puntos x1,y1, x2, y2 de graph_streets_osm_new_p_mcbo
    --
    select x1, y1, x2, y2
    into s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm
    from traffic.graph_streets_osm_new_p_mcbo
    where the_geom = geom2;
    ;

--  raise notice 'angulos % -- % -- % -- %', s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm;


    if ((not ambas_dir_mcbo) and (not ambas_dir_osm))
    then raise notice 'diff %',    ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                 s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm)) < 0.1 );

    elsif ((not ambas_dir_mcbo) and (ambas_dir_osm) )
    then raise notice 'diff %',    (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                          s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                          (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                          t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm )) 
                         ) 
                  ) < 0.1 ); 
    elsif ((ambas_dir_mcbo) and (not ambas_dir_osm))
    then raise notice 'diff %',    ((LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                         (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ) 
                        )
                 ) < 0.1 ); 
    elsif  ((ambas_dir_mcbo) and (ambas_dir_osm))
    then raise notice 'diff %',    (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                     s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                    (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                    s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                    (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                    t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm) ),
                    (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo,
                                    t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm ))
                )
          )  );
    else 
        raise notice 'diff %',    false;
    end if;

    

    
    if ((not ambas_dir_mcbo) and (not ambas_dir_osm))
    then return ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                 s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm)) < 0.1 );

    elsif ((not ambas_dir_mcbo) and (ambas_dir_osm) )
    then return (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                          s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                          (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                          t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm )) 
                         ) 
                  ) < 0.1 ); 
    elsif ((ambas_dir_mcbo) and (not ambas_dir_osm))
    then return ((LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                         (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ) 
                        )
                 ) < 0.1 ); 
    elsif  ((ambas_dir_mcbo) and (ambas_dir_osm))
    then return (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                     s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                    (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                    s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                    (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                    t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm) ),
                    (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo,
                                    t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm ))
                )
          ) < 1 );
    else 
        return false;
    end if;

end;
$$ language plpgsql;




-------------------------------------------------------------------------------------------------------------------------------

do
$$
declare
	v boolean;
begin
	v := ST_miangle ( (select the_geom
			  from traffic.graph_streets_p_mcbo
			  where id =1000203141)
			  ,
			  (select the_geom
			  from traffic.graph_streets_osm_new_p_mcbo
			  where id =508446 )
			);

-- 	raise notice 'el valor del miangle % ', v;

	v := ST_myangle ( (select the_geom
			  from traffic.graph_streets_p_mcbo
			  where id =1000203141)
			  ,
			  (select the_geom
			  from traffic.graph_streets_osm_new_p_mcbo
			  where id =508446 )
			);

	v := ST_myangle222( (select the_geom
			  from traffic.graph_streets_p_mcbo
			  where id =1000203141)
			  ,
			  (select the_geom
			  from traffic.graph_streets_osm_new_p_mcbo
			  where id =508446 )
			);

-- 	raise notice 'el valor del myangle % ', v;

end;
$$ language plpgsql;