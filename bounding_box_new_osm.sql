﻿

create table traffic.graph_streets_osm_new_mcbo () 
inherits (traffic.graph_streets_osm_new);

insert into traffic.graph_streets_osm_new_mcbo
select * 
from traffic.graph_streets_osm_new
where traffic.graph_streets_osm_new.the_geom && 
      ST_MakeEnvelope( -71.91181,10.32407 , -71.54304, 10.90291, 4326)
      ;

CREATE INDEX traffic_new_gs_mcbo_feat_idx
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (feat_id);

-- Index: traffic.traffic_new_gs_mcbo_gidx

-- DROP INDEX traffic.traffic_new_gs_mcbo_gidx;

CREATE INDEX traffic_new_gs_mcbo_gidx
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (gid);

-- Index: traffic.traffic_new_gs_mcbo_idx

-- DROP INDEX traffic.traffic_new_gs_mcbo_idx;

CREATE INDEX traffic_new_gs_mcbo_idx
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (id);

-- Index: traffic.traffic_new_gs_mcbo_max_time_catx

-- DROP INDEX traffic.traffic_new_gs_mcbo_max_time_catx;

CREATE INDEX traffic_new_gs_mcbo_max_time_catx
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (max_time_cat);

-- Index: traffic.traffic_new_gs_mcbo_sourcex

-- DROP INDEX traffic.traffic_new_gs_mcbo_sourcex;

CREATE INDEX traffic_new_gs_mcbo_sourcex
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (source);

-- Index: traffic.traffic_new_gs_mcbo_targetx

-- DROP INDEX traffic.traffic_new_gs_mcbo_targetx;

CREATE INDEX traffic_new_gs_mcbo_targetx
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (target);

-- Index: traffic.traffic_new_gs_mcbo_thegeomx

-- DROP INDEX traffic.traffic_new_gs_mcbo_thegeomx;

CREATE INDEX traffic_new_gs_mcbo_thegeomx
  ON traffic.graph_streets_osm_new_mcbo
  USING gist
  (the_geom);

-- Index: traffic.traffic_new_gss_x1

-- DROP INDEX traffic.traffic_new_gss_x1;

CREATE INDEX traffic_new_gs_mcbo_x1
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (x1);

-- Index: traffic.traffic_new_gs_mcbo_x2

-- DROP INDEX traffic.traffic_new_gs_mcbo_x2;

CREATE INDEX traffic_new_gs_mcbo_x2
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (x2);

-- Index: traffic.traffic_new_gs_mcbo_y1

-- DROP INDEX traffic.traffic_new_gs_mcbo_y1;

CREATE INDEX traffic_new_gs_mcbo_y1
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (y1);

-- Index: traffic.traffic_new_gs_mcbo_y2

-- DROP INDEX traffic.traffic_new_gs_mcbo_y2;

CREATE INDEX traffic_new_gs_mcbo_y2
  ON traffic.graph_streets_osm_new_mcbo
  USING btree
  (y2);