﻿

alter table traffic.not_in_osm add column the_geom geometry;


update traffic.not_in_osm 
set the_geom = mcbo.the_geom
from traffic.graph_streets_p_mcbo as mcbo
where traffic.not_in_osm.id = mcbo.id;


select (the_geom = (select the_geom from traffic.graph_streets_p_mcbo where id = 1000361051))
from traffic.not_in_osm
where id = 1000361051;


select (
	ST_DWithin( (select the_geom
		   from traffic.graph_streets_p_mcbo
		   where id = 1000283379), 
		   (select the_geom
		   from traffic.graph_streets_osm_new_p_mcbo
		   where id = 486406), 
		   0.00007) 
)



do
$$
begin

raise notice 'myangle %', (select (
				ST_myangle( (select the_geom
					   from traffic.graph_streets_p_mcbo
					   where id = 1000283379), 
					   (select the_geom
					   from traffic.graph_streets_osm_new_p_mcbo
					   where id = 486406)
					   )
			));
end;
$$ language plpgsql;

