﻿
-- create table traffic.graph_streets_p_mcbo () 
-- inherits (traffic.graph_streets_mcbo);

-- 
-- La sigueinte funcion elimina del mapa traffic.graph_streets_new_osm_mcbo
-- todas aquellas calles que alguno de los extremos no tiene conexion con
-- alguna otra calle
--

--
-- Esta funcion retorna true si la geometria tiene calles adyacentes
-- en cada extremo
--
create or replace function ST_hasNeighbors_navteq (geom geometry)
returns boolean as
$$
declare
	t int;
	s int;	
begin

	select target, source into t, s
	from traffic.graph_streets_p_mcbo
	where the_geom = geom;
	
	return (select (sq1.c_t > 1 and sq2.c_s > 1)
		from (select count(*) as c_t
		      from traffic.graph_streets_p_mcbo
		      where target = t or source = t) sq1 ,
		      (select count(*) as c_s
		       from traffic.graph_streets_p_mcbo
		       where source = s or target = s) sq2
		)
	;

end;
$$ language plpgsql;



--hacemos una copia de graph_treets_osm_mcbo al mapa a manipular
--
-- insert into traffic.graph_streets_p_mcbo 
-- select *
-- from traffic.graph_streets_mcbo;
-- 
-- 
select count(*)
from traffic.graph_streets_p_mcbo;

select count(*)
from traffic.graph_streets_mcbo;

-------------------------------


do
$$
declare 
	counter int; 
begin
 
    counter := -1;
   
	loop
		if counter = 0
		then exit;
		else 
			with deleted as (delete 
					 from traffic.graph_streets_p_mcbo
					 where (not ST_hasNeighbors_navteq(traffic.graph_streets_p_mcbo.the_geom))
					 RETURNING *)
			select count(*) into counter
			from deleted; 
		end if; 
	end loop; 
end;
$$ language plpgsql;


--- ------------------------------------------------------------------------------
--  
-- 
-- CREATE INDEX traffic_p_mcbo_feat_idx
--   ON traffic.graph_streets_p_mcbo
--   USING btree
--   (feat_id);
-- 
-- -- Index: traffic.traffic_p_mcbo_gidx
-- 
-- -- DROP INDEX traffic.traffic_p_mcbo_gidx;
-- 
-- CREATE INDEX traffic_p_mcbo_gidx
--   ON traffic.graph_streets_p_mcbo
--   USING btree
--   (gid);
-- 
-- -- Index: traffic.traffic_p_mcbo_idx
-- 
-- -- DROP INDEX traffic.traffic_p_mcbo_idx;
-- 
-- CREATE INDEX traffic_p_mcbo_idx
--   ON traffic.graph_streets_p_mcbo
--   USING btree
--   (id);
-- 
-- -- Index: traffic.traffic_p_mcbo_max_time_catx
-- 
-- -- DROP INDEX traffic.traffic_p_mcbo_max_time_catx;
-- 
-- CREATE INDEX traffic_p_mcbo_max_time_catx
--   ON traffic.graph_streets_p_mcbo
--   USING btree
--   (max_time_cat);
-- 
-- -- Index: traffic.traffic_p_mcbo_sourcex
-- 
-- -- DROP INDEX traffic.traffic_p_mcbo_sourcex;
-- 
-- CREATE INDEX traffic_p_mcbo_sourcex
--   ON traffic.graph_streets_p_mcbo
--   USING btree
--   (source);
-- 
-- -- Index: traffic.traffic_p_mcbo_targetx
-- 
-- -- DROP INDEX traffic.traffic_p_mcbo_targetx;
-- 
-- CREATE INDEX traffic_p_mcbo_targetx
--   ON traffic.graph_streets_p_mcbo
--   USING btree
--   (target);
-- 
-- -- Index: traffic.traffic_p_mcbo_thegeomx
-- 
-- -- DROP INDEX traffic.traffic_p_mcbo_thegeomx;
-- 
-- CREATE INDEX traffic_p_mcbo_thegeomx
--   ON traffic.graph_streets_p_mcbo
--   USING gist
--   (the_geom);
-- 
-- -- Index: traffic.traffic_new_gs_pros_x1
-- 
-- -- DROP INDEX traffic.traffic_new_gs_pros_x1;
-- 
-- CREATE INDEX traffic_p_mcbo_x1
--   ON traffic.graph_streets_p_mcbo
--   USING btree
--   (x1);
-- 
-- -- Index: traffic.traffic_p_mcbo_x2
-- 
-- -- DROP INDEX traffic.traffic_p_mcbo_x2;
-- 
-- CREATE INDEX traffic_p_mcbo_x2
--   ON traffic.graph_streets_p_mcbo
--   USING btree
--   (x2);
-- 
-- -- Index: traffic.traffic_p_mcbo_y1
-- 
-- -- DROP INDEX traffic.traffic_p_mcbo_y1;
-- 
-- CREATE INDEX traffic_p_mcbo_y1
--   ON traffic.graph_streets_p_mcbo
--   USING btree
--   (y1);
-- 
-- -- Index: traffic.traffic_p_mcbo_y2
-- 
-- -- DROP INDEX traffic.traffic_p_mcbo_y2;
-- 
-- CREATE INDEX traffic_p_mcbo_y2 ON traffic.graph_streets_p_mcbo
--   USING btree
--   (y2);
