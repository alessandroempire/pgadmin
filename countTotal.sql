﻿
do 
$$
begin
   raise notice 'Total de mcbo %', 
       (select count(*) from traffic.graph_streets_mcbo); 

   raise notice 'Total mcbo sin calles ciegas %', 
       (select count(*) from traffic.graph_streets_p_mcbo);

   raise notice 'Total osm  %', 
       (select count(*) from traffic.graph_streets_osm_new_mcbo);

   raise notice 'Total osm sin calles ciegas %', 
       (select count (*) from traffic.graph_streets_osm_new_p_mcbo);
end;
$$ language plpgsql;