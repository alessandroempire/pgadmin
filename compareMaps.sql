﻿
drop table traffic.comparedMaps;

-- create table traffic.comparedMaps(id int);
-- 
-- insert into traffic.comparedMaps
-- select mcbo.id
-- from traffic.graph_streets_p_mcbo as mcbo,
--     traffic.graph_streets_osm_new_p_mcbo as osm
-- where ST_mycompare(mcbo.the_geom, osm.the_geom)
-- ;


drop table traffic.not_in_osm;

create table traffic.not_in_osm(id int, the_geom geometry);

insert into traffic.not_in_osm
select mcbo.id, mcbo.the_geom
from traffic.graph_streets_p_mcbo as mcbo,
     traffic.graph_streets_osm_new_p_mcbo as osm
where (ST_mycompare(mcbo.the_geom, osm.the_geom))
;


select count(*)
from traffic.not_in_navteq;