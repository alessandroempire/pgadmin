

create table traffic.not_in_navteq(id int);

insert into traffic.not_in_navteq
select osm.id
from traffic.graph_streets_p_mcbo as mcbo,
     traffic.graph_streets_osm_new_p_mcbo as osm
where (not ST_mycompare(mcbo.the_geom, osm.the_geom))
;