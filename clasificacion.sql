﻿

/**
* Función que dado una geometría, lo asigna a una categoría dependiendo de su distancia. Las categorías son 0,1,2,3,4,5,6,7,8,9,10
* @param the_geom calle de la malla vial
* @return categoria de distancia
*/

drop function if exists distance_category(geometry);
create or replace function distance_category(the_geom geometry)
returns setof integer
as
$$
declare
    dist_m double precision;
begin
    select ST_Length(ST_Transform(the_geom,26986))
    into dist_m;

    return query
    select 
        case
            when dist_m < 10 then 0
            when dist_m > 11 and dist_m < 50 then 1
            when dist_m > 51 and dist_m < 100 then 2 
            when dist_m > 101 and dist_m < 200 then 3 
            when dist_m > 201 and dist_m < 400 then 4 
            when dist_m > 401 and dist_m < 800 then 5 
            when dist_m > 801 and dist_m < 1000 then 6 
            when dist_m > 1001 and dist_m < 6000 then 7 
            when dist_m > 6001 and dist_m < 10000 then 8  
            when dist_m > 10001 and dist_m < 12000 then 9 else 10
        end;
end;
$$
language plpgsql;


select count(*), sub
from (select distance_category(osm.the_geom)
	from   traffic.not_in_osm as osm) sub
group by sub
	;



select count(*), mcbo.func_class
from traffic.not_in_osm as osm,
     traffic.graph_streets_p_mcbo as mcbo
where mcbo.id = osm.id
group by mcbo.func_class
;

select count(*), mcbo.func_class
from traffic.graph_streets_p_mcbo as mcbo
group by mcbo.func_class
having mcbo.func_class <= 3
;



select count(*) from traffic.not_in_osm;

