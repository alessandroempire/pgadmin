﻿--
-- Metodo que recibe dos geometrias 
--
create or replace function ST_mycompare (geom1 geometry, geom2 geometry) 
    returns boolean as
    $$
    declare angle1 float;
        angle2 float;
    begin
        return (ST_DWithin(geom1, geom2, 0.00007) 
                and ST_myangle(geom1, geom2));
end;
$$ language plpgsql; 

--------------------------------------------------------------------------------------------------------------------------------------------------------------


create or replace function ST_angleDiff(x1 float, y1 float, x2 float, y2 float,
                    x3 float, y3 float, x4 float, y4 float)
    returns float as 
    $$
    declare
         angle1 float;
         angle2 float; 
         diff   float;
    begin
        
        select (ST_Azimuth( ST_Point(x1, y1), ST_point(x2, y2) )) into angle1;
        select (ST_Azimuth( ST_Point(x3, y3), ST_point(x4, y4) )) into angle2;
        SELECT degrees (angle2 - angle1) INTO diff;
        CASE
            WHEN diff >   180 THEN RETURN abs (diff - 360);
            WHEN diff <= -180 THEN RETURN abs (diff + 360);
            ELSE                   RETURN abs (diff);
            END CASE;
    end;
    $$ language plpgsql;

    --------------------------------------------------------------------------------------------------------------------------------------------------------------------

create or replace function ST_myangle(geom1 geometry, geom2 geometry)
    returns boolean as
    $$
    declare 
        ambas_dir_mcbo        boolean;
        ambas_dir_osm         boolean;
        s_x1_mcbo              double precision;
        s_y1_mcbo              double precision;
        t_x1_mcbo              double precision;
        t_y1_mcbo              double precision;
        s_x1_osm               double precision;
        s_y1_osm               double precision;
        t_x1_osm               double precision;
        t_y1_osm               double precision;
    begin 

        --encontrar el heading de cada geometry
        select both_dir into ambas_dir_mcbo
        from traffic.graph_streets_p_mcbo
        where the_geom = geom1;

        select both_dir into ambas_dir_osm
        from traffic.graph_streets_osm_new_p_mcbo
        where the_geom = geom2;

        --Obtener los puntos x1, y1, x2, y2 del mapa de graph_streets_mcbo
        -- x1 y1 es source
        select x1, y1, x2, y2
        into s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo
        from traffic.graph_streets_p_mcbo
        where the_geom = geom1;

        -- Obtener los puntos x1,y1, x2, y2 de graph_streets_osm_new_p_mcbo
        select x1, y1, x2, y2
        into s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm
        from traffic.graph_streets_osm_new_p_mcbo
        where the_geom = geom2;
        
--         if ((not ambas_dir_mcbo) and (not ambas_dir_osm))
--         then raise notice 'angulo %', ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
--                                      s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm))  );
-- 
--         elsif ((not ambas_dir_mcbo) and (ambas_dir_osm) )
--         then raise notice 'angulo %', (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
--                                               s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
--                               (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
--                                               t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm )) 
--                              ) 
--                       )  ); 
--         elsif ((ambas_dir_mcbo) and (not ambas_dir_osm))
--         then raise notice 'angulo %', ((LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
--                                              s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
--                              (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
--                                              s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ) 
--                             )
--                      ) ); 
--         elsif  ((ambas_dir_mcbo) and (ambas_dir_osm))
--         then raise notice 'angulo %', (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
--                                          s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
--                         (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
--                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
--                         (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
--                                         t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm) ),
--                         (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo,
--                                         t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm ))
--                     )
--               )  );
--         else 
--             raise notice 'angulo %', false;
--         end if;

        
        if ((not ambas_dir_mcbo) and (not ambas_dir_osm))
        then return ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                     s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm)) < 10 );

        elsif ((not ambas_dir_mcbo) and (ambas_dir_osm) )
        then return (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                              s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                              (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                              t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm )) 
                             ) 
                      ) < 10 ); 
        elsif ((ambas_dir_mcbo) and (not ambas_dir_osm))
        then return ((LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                             s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                             (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                             s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ) 
                            )
                     ) < 10); 
        elsif  ((ambas_dir_mcbo) and (ambas_dir_osm))
        then return (( LEAST ((ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                         s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                        (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo ,
                                        s_x1_osm, s_y1_osm, t_x1_osm, t_y1_osm) ),
                        (ST_angleDiff(s_x1_mcbo, s_y1_mcbo, t_x1_mcbo, t_y1_mcbo,
                                        t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm) ),
                        (ST_angleDiff(t_x1_mcbo, t_y1_mcbo, s_x1_mcbo, s_y1_mcbo,
                                        t_x1_osm, t_y1_osm, s_x1_osm, s_y1_osm ))
                    )
              ) < 10 );
        else 
            return false;
        end if;

end;
$$ language plpgsql;

-- do
-- $$
-- declare
--     v boolean;
-- begin
-- 
--     v := ST_myangle( (select the_geom
--                                   from traffic.graph_streets_p_mcbo
--                                   where id =1000445899)
--               ,
--                                   (select the_geom
--                                   from traffic.graph_streets_osm_new_p_mcbo
--                                   where id =507198 )
--             );
-- 
--  raise notice 'el valor del myangle % ', v;
-- 
-- end;
-- $$ language plpgsql;
