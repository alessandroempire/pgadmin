﻿

create table traffic.test(id integer);

insert into traffic.test values (1);
insert into traffic.test values (2);
insert into traffic.test values (3);
insert into traffic.test values (4);

with deleted as (delete from traffic.test RETURNING *)
select count(*) from deleted;