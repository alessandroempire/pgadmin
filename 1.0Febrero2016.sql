

--create table traffic.not_in_osm(id int);

CREATE INDEX traffic_comparedMaps_idx
  ON traffic.comparedMaps
  USING btree
  (id);

insert into traffic.not_in_osm
select mcbo.id 
from traffic.graph_streets_p_mcbo as mcbo 
where mcbo.id not in (select id from traffic.comparedMaps)
;
